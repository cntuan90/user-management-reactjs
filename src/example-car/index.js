import React, { Component } from "react";

export default class ExampleCar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      img: "./img/imgBlackCar.jpg",
    };
  }

  changeColor = (img) => {
    this.setState({
      img,
    });
  };

  render() {
    const { img } = this.state;
    return (
      <div className="container">
        <h3>*ExampleCar</h3>
        <div className="row">
          <div className="col-md-6">
            <img className="img-fluid" src={img} alt="" />
          </div>
          <div className="col-md-6">
            <button
              onClick={() => {
                this.changeColor("./img/imgRedCar.jpg");
              }}
              className="btn btn-danger"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.changeColor("./img/imgSilverCar.jpg");
              }}
              className="btn btn-light mx-2"
            >
              Silver
            </button>
            <button
              onClick={() => {
                this.changeColor("./img/imgBlackCar.jpg");
              }}
              className="btn btn-dark"
            >
              Đen{" "}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
