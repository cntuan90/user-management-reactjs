import React, { Component } from "react";

export default class ExampleHandlingEvent extends Component {
  isLogin = false;

  handleClick = () => {
    this.isLogin = true;
  };
  showInfo = () => {
    return this.isLogin ? (
      <h1>Hello CyberSoft!!!</h1>
    ) : (
      <button className="btn btn-success" onClick={this.handleClick}>
        Login
      </button>
    );
  };

  render() {
    return (
      <div>
        <h3>*ExampleHandlingEvent</h3>
        {this.showInfo()}
      </div>
    );
  }
}
