import React, { Component } from "react";

export default class Child extends Component {
  handleReset = () => {
    const resetUser = "Nguyen";
    this.props.getResetUsername(resetUser);
  };
  render() {
    return (
      <div>
        <h3>*Child</h3>
        <p>
          Username: {this.props.data} - Lop: {this.props.lop}
        </p>
        <button className="btn btn-info" onClick={this.handleReset}>
          Reset Username
        </button>
      </div>
    );
  }
}
