import React, { Component } from "react";
import Child from "./child";
import ChildFunction from "./childFunction";
import DemoChildren from "./children";

export default class Props extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "Nguyen",
      lop: "BC03",
    };
  }

  handleChangeUsername = () => {
    this.setState({
      userName: "Cybersoft",
      lop: "BC03-Alpha",
    });
  };

  handleGetResetUsername = (resetUsername) => {
    console.log(resetUsername);
    this.setState({
      userName: resetUsername,
    });
  };
  render() {
    return (
      <div>
        <h3>*Props</h3>
        <p>
          Username: {this.state.userName} - Lop: {this.state.lop}
        </p>
        <button className="btn btn-success" onClick={this.handleChangeUsername}>
          Change Username
        </button>
        <Child
          getResetUsername={this.handleGetResetUsername}
          data={this.state.userName}
          lop={this.state.lop}
        />
        <ChildFunction data={this.state.userName} lop={this.state.lop} />
        <DemoChildren>
          <div>
            <p>Demo Children</p>
            <p>Loremmmmm</p>
          </div>
        </DemoChildren>
      </div>
    );
  }
}
