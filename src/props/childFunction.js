import React from "react";

export default function ChildFunction(props) {
  return (
    <div>
      <h4>*ChildFunction</h4>
      <p>
        Username: {props.data} - Lop: {props.lop}
      </p>
    </div>
  );
}
