import React, { Component } from "react";
import SanPham from "./san-pham";

export default class DanhSachSanPham extends Component {
  renderListProduct = () => {
    const { listProduct } = this.props;
    return listProduct.map((item) => {
      return (
        <SanPham
          key={item.maSP}
          product={item}
          getDetailProduct={this.props.getDetailProduct}
          getCartItem={this.props.getCartItem}
        />
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderListProduct()}</div>
      </div>
    );
  }
}
