import React, { Component } from "react";
import DanhSachSanPham from "./danh-sach-san-pham";
import Modal from "./modal";
import data from "./data.json";

export default class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProduct: data,
      detailProduct: data[0],
      listCart: [],
    };
  }

  showNumberOfProduct = () => {
    let total = 0;
    this.state.listCart.forEach((item) => {
      total += item.soLuong;
      console.log(total);
    });
    return total;
  };

  handleGetDetailProduct = (product) => {
    console.log(product);
    this.setState({
      detailProduct: product,
    });
  };

  _findIndex = (maSP) => {
    return this.state.listCart.findIndex((item) => {
      return item.maSP === maSP;
    });
  };

  handleGetCartItem = (product) => {
    let productNew = {
      maSP: product.maSP,
      tenSP: product.tenSP,
      hinhAnh: product.hinhAnh,
      giaBan: product.giaBan,
      soLuong: 1,
    };

    let listCart = [...this.state.listCart];

    let index = this._findIndex(productNew.maSP);

    index !== -1
      ? (listCart[index].soLuong += 1)
      : (listCart = [...this.state.listCart, productNew]);

    this.setState({
      listCart,
    });
  };

  handleDeleteProduct = (product) => {
    let index = this._findIndex(product.maSP);

    let listCart = [...this.state.listCart];

    if (index !== -1) listCart.splice(index, 1);

    this.setState({
      listCart,
    });
  };

  handleUpdateProductNumber = (item, status) => {
    let index = this._findIndex(item.maSP);
    let listCart = [...this.state.listCart];

    status === true
      ? (listCart[index].soLuong += 1)
      : (listCart[index].soLuong -= 1);

    if (listCart[index].soLuong === 0) listCart.splice(index, 1);
    this.setState({
      listCart,
    });
  };

  render() {
    // console.log(this.state.listCart);
    const { detailProduct } = this.state;
    const { listCart } = this.state;

    return (
      <div>
        <h3 className="title">Bài tập giỏ hàng</h3>
        <div className="container">
          <button
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#modelId"
          >
            Giỏ hàng ({this.showNumberOfProduct()})
          </button>
        </div>
        <DanhSachSanPham
          listProduct={this.state.listProduct}
          getDetailProduct={this.handleGetDetailProduct}
          getCartItem={this.handleGetCartItem}
        />
        <Modal
          listCart={listCart}
          getProductDelete={this.handleDeleteProduct}
          getUpdateProductNumber={this.handleUpdateProductNumber}
        />
        <div className="row">
          <div className="col-sm-5">
            <img className="img-fluid" src={detailProduct.hinhAnh} alt="" />
          </div>
          <div className="col-sm-7">
            <h3>Thông số kỹ thuật</h3>
            <table className="table">
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{detailProduct.manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{detailProduct.heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{detailProduct.cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera sau</td>
                  <td>{detailProduct.cameraSau}</td>
                </tr>
                <tr>
                  <td>RAM</td>
                  <td>{detailProduct.ram}</td>
                </tr>
                <tr>
                  <td>ROM</td>
                  <td>{detailProduct.rom}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
