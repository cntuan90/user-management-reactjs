import React, { Component } from "react";

export default class State extends Component {
  isLogin = false;
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
    };
  }

  handleClick = () => {
    this.setState({
      isLogin: true,
    });
  };

  showInfo = () => {
    return this.state.isLogin ? (
      <h1>Hello CyberSoft!!!</h1>
    ) : (
      <button className="btn btn-success" onClick={this.handleClick}>
        Login
      </button>
    );
  };

  render() {
    console.log("render");
    return (
      <div>
        <h3>*State</h3>
        {this.showInfo()}
      </div>
    );
  }
}
