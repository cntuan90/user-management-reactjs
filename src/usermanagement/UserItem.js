import React, { Component } from "react";

class UserItem extends Component {
  renderUserList = (userList) => {
    return userList.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.name}</td>
          <td>{item.username}</td>
          <td>{item.email}</td>
          <td>{item.phoneNumber}</td>
          <td>{item.type}</td>
          <td>
            <button
              className="btn btn-info mr-2"
              data-toggle="modal"
              data-target="#modelIdUser"
              onClick={() => {
                this.props.getDetailUser(item);
              }}
            >
              Edit
            </button>
            <button
              onClick={() => {
                this.props.getUserDelete(item);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    const { userList } = this.props;
    return <>{this.renderUserList(userList)}</>;
  }
}

export default UserItem;
