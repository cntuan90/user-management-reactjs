import React, { Component } from "react";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      username: "",
      email: "",
      phoneNumber: "",
      type: "USER",
    };
    this.close = React.createRef();

    console.log("constructor - chỉ chạy 1 lần");
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps && nextProps.detailUser) {
      this.setState({
        id: nextProps.detailUser.id,
        name: nextProps.detailUser.name,
        username: nextProps.detailUser.username,
        phoneNumber: nextProps.detailUser.phoneNumber,
        email: nextProps.detailUser.email,
        type: nextProps.detailUser.type,
      });
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.close.current.click();

    this.props.getCreateUser(this.state);
  };

  handleOnChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelIdUser"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {this.props.detailUser ? "EDIT USER" : "ADD USER"}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                ref={this.close}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label>Name</label>
                  <input
                    onChange={this.handleOnChange}
                    name="name"
                    type="text"
                    className="form-control"
                    value={this.state.name}
                  />
                </div>
                <div className="form-group">
                  <label>Username</label>
                  <input
                    onChange={this.handleOnChange}
                    name="username"
                    type="text"
                    className="form-control"
                    value={this.state.username}
                  />
                </div>
                <div className="form-group">
                  <label>Email</label>
                  <input
                    onChange={this.handleOnChange}
                    name="email"
                    type="text"
                    className="form-control"
                    value={this.state.email}
                  />
                </div>
                <div className="form-group">
                  <label>Phone Number</label>
                  <input
                    onChange={this.handleOnChange}
                    name="phoneNumber"
                    type="text"
                    className="form-control"
                    value={this.state.phoneNumber}
                  />
                </div>
                <div className="form-group">
                  <label>Type</label>
                  <select
                    onChange={this.handleOnChange}
                    name="type"
                    className="form-control"
                    value={this.state.type}
                  >
                    <option>USER</option>
                    <option>VIP</option>
                  </select>
                </div>
                <button type="submit" className="btn btn-success">
                  {this.props.detailUser ? "UPDATE" : "SUBMIT"}
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
