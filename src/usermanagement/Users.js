import React, { Component } from "react";
import UserItem from "./UserItem";

class Users extends Component {
  render() {
    const { userList } = this.props;
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            <UserItem
              userList={userList}
              getUserDelete={this.props.getUserDelete}
              getDetailUser={this.props.getDetailUser}
            />
          </tbody>
        </table>
      </div>
    );
  }
}

export default Users;
