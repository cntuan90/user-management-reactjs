import React, { Component } from "react";
import Search from "./Search";
import Users from "./Users";
import Modal from "./Modal";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [
        {
          id: 1,
          name: "Dinh Phuc Nguyen",
          username: "dpnguyen",
          email: "dpnguyen@gmail.com",
          phoneNumber: "1123123213",
          type: "VIP",
        },
        {
          id: 2,
          name: "hao",
          username: "nguyendp",
          email: "nguyendp@gmail.com",
          phoneNumber: "1123123213",
          type: "VIP",
        },
      ],
      detailUser: null,
    };
  }

  handleCreateUser = (user) => {
    if (user.id) {
      let userList = [...this.state.userList];

      let index = this._findIndex(user.id);

      if (index !== -1) userList.splice(index, 1);
      userList.push(user);

      this.setState({
        userList,
      });
    } else {
      const newUser = { ...user, id: Math.random() };

      let userList = [...this.state.userList, newUser];

      this.setState({
        userList,
      });
    }
  };

  _findIndex = (id) => {
    return this.state.userList.findIndex((item) => {
      return item.id === id;
    });
  };

  handleDeleteUser = (user) => {
    let index = this._findIndex(user.id);

    let userList = [...this.state.userList];

    if (index !== -1) userList.splice(index, 1);

    this.setState({
      userList,
    });
  };

  handleGetDetailUser = (user) => {
    console.log(user);
    this.setState({
      detailUser: user,
    });
  };

  handleUpdateUser = (user) => {
    let userList = [...this.state.userList];

    let index = this._findIndex(user.id);

    if (index !== -1) userList.splice(index, 1);
    userList.push(user);

    this.setState({
      userList,
    });
  };

  handleSearchUser = (userList) => {
    if (userList) {
      this.setState({
        userList,
      });
    } else {
      this.setState({
        userList: [
          {
            id: 1,
            name: "Dinh Phuc Nguyen",
            username: "dpnguyen",
            email: "dpnguyen@gmail.com",
            phoneNumber: "1123123213",
            type: "VIP",
          },
          {
            id: 2,
            name: "hao",
            username: "nguyendp",
            email: "nguyendp@gmail.com",
            phoneNumber: "1123123213",
            type: "VIP",
          },
        ],
      });
    }
  };

  render() {
    return (
      <div className="container d-block">
        <h1 className="display-4 text-center my-3">User Management</h1>
        <div className="d-flex justify-content-between align-items-center">
          <Search
            userList={this.state.userList}
            searchUser={this.handleSearchUser}
          />
          <button
            className="btn btn-success"
            data-toggle="modal"
            data-target="#modelIdUser"
            onClick={() => {
              this.setState({
                detailUser: null,
              });
            }}
          >
            Add User
          </button>
        </div>
        <Users
          userList={this.state.userList}
          getUserDelete={this.handleDeleteUser}
          getDetailUser={this.handleGetDetailUser}
        />
        <Modal
          getCreateUser={this.handleCreateUser}
          userList={this.state.userList}
          detailUser={this.state.detailUser}
          getUpdateUser={this.handleUpdateUser}
        />
      </div>
    );
  }
}

export default Home;
