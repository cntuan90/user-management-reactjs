import React, { Component } from "react";

class Search extends Component {
  handleOnChange = (event) => {
    const { value } = event.target;

    let keyword = value;
    keyword = keyword.toLowerCase().trim();
    let result = [];

    this.props.userList.forEach((item) => {
      let convertedFullName = item.name.toLowerCase();
      if (item.id === keyword || convertedFullName.includes(keyword)) {
        result.push(item);
        return result;
      }
    });
    if (keyword === "") result = false;
    this.props.searchUser(result);
  };

  render() {
    return (
      <input
        onChange={this.handleOnChange}
        type="text"
        className="form-control mb-3 w-50"
      />
    );
  }
}

export default Search;
