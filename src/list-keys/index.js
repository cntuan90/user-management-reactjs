import React, { Component } from "react";

export default class ListKeys extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProduct: [
        { id: 1, name: "Iphone", price: 123 },
        { id: 2, name: "HTC", price: 456 },
        { id: 3, name: "Nokia", price: 789 },
      ],
    };
  }

  renderTable = () => {
    const { listProduct } = this.state;
    return listProduct.map((product) => {
      return (
        <tr key={product.id}>
          <td>{product.id}</td>
          <td>{product.name}</td>
          <td>{product.price}</td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <h3>*ListKeys</h3>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
