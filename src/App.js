import "./App.css";
import ExampleCar from "./example-car";
import HandlingEvent from "./handling-event";
import ExampleHandlingEvent from "./handling-event/example";
// import Baitap1 from "./baitap1";
// import Baitap2 from "./baitap2";
import RenderingElements from "./rendering-elements";
import State from "./state";
import ListKeys from "./list-keys/index";
import ExampleList from "./list-keys/example";
import Props from "./props";
import ShoppingCart from "./shopping-cart";
import Home from "./usermanagement";

function App() {
  return (
    <div>
      {/* <Baitap1 /> */}
      {/* <Baitap2 /> */}
      {/* <RenderingElements />
      <hr />
      <HandlingEvent />
      <hr />
      <ExampleHandlingEvent />
      <hr />
      <State />
      <hr />
      <ExampleCar />
      <hr />
      <ListKeys />
      <ExampleList />
      <hr />
      <Props />
      <hr />
      <ShoppingCart />
      <hr /> */}
      <Home />
    </div>
  );
}

export default App;
